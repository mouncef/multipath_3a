clc
close all

el = zeros(40,1);
for sat_index = 1:40
cell_len_slice_time = cell_len_slice{sat_index, 1};


%This loop is used to calculate the time axis 
for i=2:length(cell_len_slice{sat_index,1})
    cell_len_slice_time(i) = cell_len_slice_time(i) + cell_len_slice_time(i-1);
end


%------------------------------------------------------
%This loop plots the values of mpn for every slice of the first satellite
%The static slices are in green and the dynamic ones are in red
%Although we have some outliers in the mpn
%Those values can be seen clearly in the plot
%They influence the scale of the mpn axis and make the graph not as visible
%as it should be
le = size(cell_len_slice_time);


% t = linspace(1,cell_len_slice_time(1) ,b(1:1));
window = 5;
finish = 10;


for i=round(1 +((le(1:1) - 1)/100*(finish-window))):(round(le(1:1) - 1)/100*finish)
   el1 = table2array(data_slice{i,1}(:,16));
end
el(sat_index) = sum(el1)/size(el1,1);



% if(el(sat_index) < 11)
figure
for i=round(1 +((le(1:1) - 1)/100*(finish-window))):(round(le(1:1) - 1)/100*finish)
   b =  size(data_slice{i,1}(:,4));
   data = table2array(data_slice{i,1}(:,4));


   t = linspace(cell_len_slice_time(i), cell_len_slice_time(i+1) ,b(1:1));

   if cell_state_slice{1,1}(i) == 0 
        plot(t, data, 'g' ); hold on;
   else 
        plot(t, data, 'r' ); hold on;
   end
   title("MPN values of the data slices(static (green) and dynamic (red)")
   xlabel('Time (sec)'); ylabel('MPN (m)');
end
% end

%------------------------------------------------------







%------------------------------------------------------
%What I can try and what would be interesting is to calculate the average
%MPN of each slice and plot it with respect of time
%We can still see the problem of outliers


for i=1:(le(1:1))
       data = table2array(data_slice{i,1}(:,4));
        cc(i) = mean(data);        
end


% figure
% plot(cell_len_slice_time, cc);






%------------------------------------------------------







%------------------------------------------------------

% find outliers

% for i=1:311
% 
%  for j=1:size(data_slice{i,1}(:,4))
%      cc = table2array(data_slice{i,1}(:,4));
%     if cc(j) > 10
%         a = i
%     end
%  end
% 
% end

%------------------------------------------------------

%-----------------------------------------------------
%ignore this
%This is just me testing some things 

%a =[1 2 1 2 1 2 1 2];
%t =[1 2 3 4 5 6 7 8];


% for i=1:length(a)
%     if i<=4
%         plot(t(1:i),a(1:i), 'g' ); hold on;
% 
%     else
%         plot(t(4:i),a(4:i), 'r' ); hold on;
%     end
% end

hold off;
end
