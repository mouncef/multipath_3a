% Check if process is paused
function funs = app_fun
  funs.pause = @pause;
  funs.pause1 = @pausse1;
  funs.plot_mpn = @plot_mpn;
  funs.plot_mpnr = @plot_mpnr;
  funs.plot_user_speed = @plot_user_speed;


end

function jj = pause(jj, m)
       drawnow();
       jj = jj+1;
        if m.PauseButton.Value
                    waitfor(m.PauseButton,'Value',false)
                    jj = m.Slider.Value;
        end
        
end


function jj = pause1(jj, m)
       drawnow();
        if m.PauseButton.Value
                    waitfor(m.PauseButton,'Value',false)
                    jj = m.Slider.Value;
        end
end


function plot_mpn(m, jj, step_plot, num_slice, temp_data_slice, str_state, temp_num_plot, st)
    title(st, sprintf('Sliced data (%s, plotted %d of %d)', str_state, temp_num_plot, num_slice));

    for i=1:step_plot:num_slice
       plot(st, temp_data_slice{i}.tow - temp_data_slice{i}.tow(1), temp_data_slice{i}.mpn); hold(st, 'on') ;                 
    end
    pause1(jj, m)

    xlabel(st, 'Time (sec)'); ylabel(st,'MPN (m)');
    hold(st, 'off')
end





function plot_mpnr(m, jj, step_plot, num_slice, temp_data_slice, str_state, temp_num_plot, st)
    title(st, sprintf('Sliced data (%s, plotted %d of %d)', str_state, temp_num_plot, num_slice));

    for i=1:step_plot:num_slice
       plot(st, temp_data_slice{i}.tow - temp_data_slice{i}.tow(1), temp_data_slice{i}.mpn); hold(st, 'on') ;                 
    end
    pause1(jj, m)

    xlabel(st, 'Time (sec)'); ylabel(st,'MPN (m)');
    hold(st, 'off')
end






function plot_user_speed(m, jj, step_plot, num_slice, temp_data_slice, str_state, temp_num_plot, st)
    title(st, sprintf('Sliced data (%s, plotted %d of %d)', str_state, temp_num_plot, num_slice));

    for i=1:step_plot:num_slice
        plot(st, temp_data_slice{i}.tow - temp_data_slice{i}.tow(1), temp_data_slice{i}.speed*3.6); hold(st, 'off');

    end
    pause1(jj, m)

    xlabel(st,'Time (sec)'); ylabel(st,'User speed (km/h)');
    hold(st, 'off')
end
